import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChildParentRoutingModule } from './child-parent-routing.module';
import { CpParentComponent } from './cp-parent/cp-parent.component';
import { CpChildComponent } from './cp-child/cp-child.component';

@NgModule({
  declarations: [CpParentComponent, CpChildComponent],
  imports: [
    CommonModule,
    ChildParentRoutingModule
  ]
})
export class ChildParentModule { }
