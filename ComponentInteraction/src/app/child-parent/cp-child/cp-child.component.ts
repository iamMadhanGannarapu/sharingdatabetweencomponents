import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-cp-child',
  templateUrl: './cp-child.component.html',
  styleUrls: ['./cp-child.component.css']
})
export class CpChildComponent implements OnInit {

  @Output() public childEvent = new EventEmitter();

  constructor() { }
  sendToParent() {
    this.childEvent.emit("hey i'm from child component");

  }
  ngOnInit() {
  }

}
