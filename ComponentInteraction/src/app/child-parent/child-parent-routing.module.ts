import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CpParentComponent } from './cp-parent/cp-parent.component';
import { CpChildComponent } from './cp-child/cp-child.component';

const routes: Routes = [
  { path: "parent", component: CpParentComponent },
  { path: "child", component: CpChildComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChildParentRoutingModule { }
