import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cp-parent',
  templateUrl: './cp-parent.component.html',
  styleUrls: ['./cp-parent.component.css']
})
export class CpParentComponent implements OnInit {
  public message = "";
  constructor() { }

  ngOnInit() {
  }

}
