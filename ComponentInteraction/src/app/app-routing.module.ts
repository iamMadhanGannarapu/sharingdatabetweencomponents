import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import {  } from '../app/parent-child/parent-child.module/';


const routes: Routes = [
  { path: "parent", loadChildren: "../app/parent-child/parent-child.module#ParentChildModule" },
  { path: "child", loadChildren: "../app/child-parent/child-parent.module#ChildParentModule" },
  { path: "viewChild", loadChildren: "../app/child-parent-viewchild/child-parent-viewchild.module#ChildParentViewchildModule" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
