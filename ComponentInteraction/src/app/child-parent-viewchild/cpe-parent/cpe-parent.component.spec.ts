import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpeParentComponent } from './cpe-parent.component';

describe('CpeParentComponent', () => {
  let component: CpeParentComponent;
  let fixture: ComponentFixture<CpeParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpeParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpeParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
