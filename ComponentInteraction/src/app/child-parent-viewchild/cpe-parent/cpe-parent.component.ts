import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { CpeChildComponent } from '../cpe-child/cpe-child.component'

@Component({
  selector: 'app-cpe-parent',
  templateUrl: './cpe-parent.component.html',
  styleUrls: ['./cpe-parent.component.css']
})
export class CpeParentComponent implements OnInit, AfterViewInit {
  @ViewChild(CpeChildComponent) child;
  constructor() { }
  message: string;

  ngAfterViewInit() {
    this.message = this.child.message
  }
  ngOnInit() {
  }

}
