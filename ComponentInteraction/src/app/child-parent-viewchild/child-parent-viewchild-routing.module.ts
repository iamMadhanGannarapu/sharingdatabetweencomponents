import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CpeParentComponent } from './cpe-parent/cpe-parent.component';
import { CpeChildComponent } from './cpe-child/cpe-child.component';

const routes: Routes = [
  { path: "parent", component: CpeParentComponent },
  { path: "child", component: CpeChildComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChildParentViewchildRoutingModule { }
