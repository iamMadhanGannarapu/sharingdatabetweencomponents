import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpeChildComponent } from './cpe-child.component';

describe('CpeChildComponent', () => {
  let component: CpeChildComponent;
  let fixture: ComponentFixture<CpeChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpeChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpeChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
