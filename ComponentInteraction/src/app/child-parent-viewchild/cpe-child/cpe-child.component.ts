import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cpe-child',
  templateUrl: './cpe-child.component.html',
  styleUrls: ['./cpe-child.component.css']
})
export class CpeChildComponent implements OnInit {
  message = "hey i'm from child componet";
  constructor() { }

  ngOnInit() {
  }

}
