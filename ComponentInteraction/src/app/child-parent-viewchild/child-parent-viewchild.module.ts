import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChildParentViewchildRoutingModule } from './child-parent-viewchild-routing.module';
import { CpeParentComponent } from './cpe-parent/cpe-parent.component';
import { CpeChildComponent } from './cpe-child/cpe-child.component';

@NgModule({
  declarations: [CpeParentComponent, CpeChildComponent],
  imports: [
    CommonModule,
    ChildParentViewchildRoutingModule
  ]
})
export class ChildParentViewchildModule { }
